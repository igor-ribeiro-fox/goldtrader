<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Oferta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Offer', function(Blueprint $table){

            $table->increments('id');
            $table->string('Name');
            $table->string('Goods');
            $table->string('Quantity');
            $table->string('Form');
            $table->string('Purity');
            $table->string('Amount');
            $table->string('BankCustody');
            $table->string('Contract');
            $table->string('Price');
            $table->string('DiscountGross');
            $table->string('DiscountNet');
            $table->string('CommissionSellerSide');
            $table->string('CommissionBuyerSide');
            $table->string('DeliveryTerms');
            $table->string('BankGuarantee');
            $table->string('Payment');
            $table->string('Procedures');
            $table->string('TransactionCode');
            $table->string('BuyerCode');
            $table->string('SellerCode');
            $table->string('SellerId');
            $table->timestamp('Created_at');
            $table->timestamp('Validity');
            $table->timestamp('Reopen_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Offer');
    }
}
