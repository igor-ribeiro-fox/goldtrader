<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Client extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Client', function (Blueprint $table){
            $table->increments('id');
            $table->string('Country');
            $table->string('FullAddress');
            $table->string('CompanyName');
            $table->string('RegistrationNumber');
            $table->string('Email');
            $table->string('Tel1');
            $table->string('Tel2');
            $table->string('BankName');
            $table->string('BankBranch');
            $table->string('BankAccount');
            $table->string('IbanCode');
            $table->string('SwiftCode');
            $table->string('Representative');
            $table->string('Mandate');
            $table->string('Observations');
            $table->timestamp('Created_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
